# frozen_string_literal: true

require_relative "lib/model_query/version"

Gem::Specification.new do |spec|
  spec.name = "model_query"
  spec.version = ModelQuery::VERSION
  spec.authors = ["Bevan Holborn"]
  spec.summary = "Write a short summary, because RubyGems requires one."
  spec.files = Dir["lib/**/*.rb"] + Dir["bin/*"]

  spec.add_runtime_dependency 'activesupport', '>= 7.0'
end
