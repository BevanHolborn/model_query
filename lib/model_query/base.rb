module ModelQuery
  class Base

    private attr_reader :relation

    def initialize(relation = self.class.query_model.all)
      @relation = relation
    end

    def resolve(*args, **kwargs) = raise NotImplementedError

    class << self

      attr_accessor :query_model_name

      def [](model)
        Class.new(self).tap do |klass|
          klass.query_model_name = model.name
        end
      end

      def query_model_name
        @query_model_name ||= name.sub(/::[^\:]+$/, "")
      end

      def query_model
        query_model_name.safe_constantize
      end

      def resolve(*args, **kwargs) = new.resolve(*args, **kwargs)
      alias_method :call, :resolve
    end

  end
end